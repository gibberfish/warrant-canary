-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Gibberfish, Inc. Warrant Canary

As of September 6 2020, Gibberfish, Inc. has never received any:

1) National Security Letters;
2) Foreign Intelligence Surveillance Act orders;
3) Search warrants, subpoenas, or any other document purporting to require Gibberfish, Inc. to provide documents, information, or testimony in any criminal proceeding;
4) Requests for documents or information regarding our clients or their data, of any kind, from any government agency;
5) Requests to preserve documents by third-parties or government agencies; or
6) Subpoenas, or any other document purporting to require Gibberfish, Inc. to provide documents, information, or testimony in any civil proceeding.

In addition, we have never placed any backdoors or other deliberate vulnerabilities in our hardware or software, and have not received any requests to do so.

If this message is not posted again on or before October 3, 2020, signed with our Gibberfish Security PGP key (Primary key fingerprint: E3D0 214D 7E24 16E6 93EE  B190 C438 28F3 64C5 18FA), then it should be assumed that one or more of these representations are no longer true. Similarly, if this warrant canary is posted again and there are any material differences in the representations made, it should be assumed that this change is deliberate and that the representations have changed accordingly.

The following links can be used to confirm that this message was not generated at an earlier date:

1. https://www.reuters.com/article/us-global-race-protests-portland/police-use-tear-gas-portland-protesters-throw-fire-bombs-idUSKBN25X08R
2. https://ktla.com/news/california/200-people-rescued-by-helicopter-in-mammoth-pools-area-as-creek-fire-spreads-in-sierra-national-forest/
3. https://www.theguardian.com/world/2020/sep/06/hong-kong-protesters-oppose-election-delay-arrests

Instructions for verifying this message can be found at https://gibberfish.org/verify-warrant-canaries/

Note: As of June 1, 2019 we are switching to using subkeys for cryptographic signing. Please import the updated public key from https://download.gibberfish.org/canary/security.pubkey before verifying the signature on this document.

-----BEGIN PGP SIGNATURE-----

iQJMBAEBCgA2FiEEKpBc6nBZbKy/D8oEVbdKerXjxFsFAl9VDDIYHHNlY3VyaXR5
QGdpYmJlcmZpc2gub3JnAAoJEFW3Snq148RbkeQP/3x7sF3C6ViH1oHg0RP7rx0+
uftxRfZYhBeeDPPx4Pfor/Drwudf/pVC1YduukNAJbFLDEEHFpBVCLdItmSVM69O
MoBKMLOpW7YO913pK4INkk7CdyR1UbgiAMtBIveXmGp1FwkXrFNDjZZnbNlEOMLa
go1l5eINWmcChNKbAiLNl70qzmTfGqqxe0+CgfiWq3u+Y24jn5qtepMvr/cg4R1G
u4GNFHYBT/DaZ01c/2x7Ql5eT/3UqAnR6lkJOiwAxf5gZ1x+j3e02MxBX24adRWa
nxfX4Bj+aBUHrkNrm+tfrA8cbC+tMtEaVLMsNL0oJoRGOSb9UmPMkolYPVI+fr8M
r9u4EgEP9QHmdtN4TluSpL0gGUm3QRa4XN25qSv82oBeJ7YCfNHNKS1EyIviEJ8K
skwpIRfP4Ejg0OjWsv65p65NdwBVpgAGR3x+AmbbY+VOO1mb+Ht25qJm3KW7uwnq
IHLS0oFWM1Emuk7JXkIyk9TlDL9rQ/sNJ5otyeovLGcjWuVWarOdSaGUa49MPL2j
zbgnvN4eBO/nHl34QnowVQrutvTjnmsBnLNH3sfdO1TNRXAXTNbK+5/c2aXO38OW
Cm7vJhnlY66avV1haCjEM3HZtwnvIAt1vy6FxCj3y871LSCe5vUkdgNxkwkWMAlS
Ujluw5xzLFwDPj78Ibu6
=Asbc
-----END PGP SIGNATURE-----
