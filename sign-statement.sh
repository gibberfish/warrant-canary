#!/usr/bin/env bash

# sign canary statement
gpg2 --clearsign --textmode --local-user security@gibberfish.org --digest-algo=sha512 "${1}"

# verify signature
gpg2 --verify "${1}".asc
