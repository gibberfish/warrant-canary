-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Gibberfish, Inc. Warrant Canary

As of March 1, 2018, Gibberfish, Inc. has never received any:

1) National Security Letters;
2) Foreign Intelligence Surveillance Act orders;
3) Search warrants, subpoenas, or any other document purporting to require Gibberfish, Inc. to provide documents, information, or testimony in any criminal proceeding;
4) Requests for documents or information regarding our clients or their data, of any kind, from any government agency;
5) Requests to preserve documents by third-parties or government agencies; or
6) Subpoenas, or any other document purporting to require Gibberfish, Inc. to provide documents, information, or testimony in any civil proceeding.

In addition, we have never placed any backdoors or other deliberate vulnerabilities in our hardware or software, and have not received any requests to do so.

If this message is not posted again on or before April 7, 2018, signed with our Gibberfish Security PGP key (E3D0214D7E2416E693EEB190C43828F364C518FA), then it should be assumed that one or more of these representations are no longer true. Similarly, if this warrant canary is posted again and there are any material differences in the representations made, it should be assumed that this change is deliberate and that the representations have changed accordingly.

The following links can be used to confirm that this message was not generated at an earlier date:

1. https://www.nytimes.com/2018/03/01/business/trump-tariffs.html
2. https://gizmodo.com/frances-favorite-crypto-fascist-could-face-three-years-1823423064
3. http://www.bbc.com/news/world-europe-43239331

Instructions for verifying this message can be found at https://gibberfish.org/verify-warrant-canaries/

-----BEGIN PGP SIGNATURE-----

iQIzBAEBCgAdFiEE49AhTX4kFuaT7rGQxDgo82TFGPoFAlqYc9AACgkQxDgo82TF
GPqGBBAApR/ZtM+ftq7G21IJIWbqZiHkZg/ATlKBiSHkzeYECQcE3ahPLNNcH9sc
wfqkzFgQ6pi74pWhIgzuluwUU92YYWFl6mNx/KSLdwNpL2whNrs8SECaeJQKS4wu
ISR99bpThSwuJjqetq3IHkmstWS04UQuu1D3j+fkZUMICO0vQk8TtDoiUQvs6/73
FrpvxNHHhgP5IVn4c+nj37LNCiXY1JNTrxHpU4HEPbQOcNzSasmCRkb2wISJr12x
lkmZ+kS/2lwAOJ6YfTRjxgDT93D/2YVmgp7xfUZcl3BrGfovDGJpT5VtXWkKVOiz
9B1yitXbREGZ931HX2rhj0iCGZYvu97jRoDTVI1feXrKAF3FLy5u9rOvU59WyyBd
VeI5eSCV4ao0Tzua//CSK06oTnHrgwMp3pyexze7xynoe/c2Ya3Xxplt/HKJ9Tt2
oVog40Djnw9XMW2+orSkqdN1coywWZu1DSj/Us1/KhVDEt+D4eNV1S774jQxZH7N
adTzN83VWPf2BqDiwqmtQnPzLoL+ABNnD1COEAkeOuPufsn0VPRhLm87RkcUrv9G
VEYrB86NLPt/dyD/6Q95b0DNUqRx5TBf6/csKRFg6pKKvsvkkruJI9VlVGOSfWLg
uFHaQFvwOn2Lnsqzo17H+Ljsjwb5TYlxFqINjsFtchr/SBtoZqM=
=/jUY
-----END PGP SIGNATURE-----
