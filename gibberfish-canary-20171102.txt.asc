-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Gibberfish, Inc. Warrant Canary

As of November 2, 2017, Gibberfish, Inc. has never received any:

1)            National Security Letters;
2)            Foreign Intelligence Surveillance Act orders;
3)            Search warrants, subpoenas, or any other document purporting to require Gibberfish, Inc. to provide documents, information, or testimony in any criminal proceeding;
4)            Requests for documents or information regarding our clients or their data, of any kind, from any government agency;
5)            Requests to preserve documents by third-parties or government agencies; or
6)            Subpoenas, or any other document purporting to require Gibberfish, Inc. to provide documents, information, or testimony in any civil proceeding.

In addition, we have never placed any backdoors or other deliberate vulnerabilities in our hardware or software, and have not received any requests to do so.

If this message is not posted again on or before December 2, 2017, signed with our Gibberfish Security PGP key (E3D0214D7E2416E693EEB190C43828F364C518FA), then it should be assumed that one or more of these representations are no longer true. Similarly, if this warrant canary is posted again and there are any material differences in the representations made, it should be assumed that this change is deliberate and that the representations have changed accordingly.

The following links can be used to confirm that this message was not generated at an earlier date:

1. http://www.cnn.com/2017/11/02/europe/uk-may-fallon-westminster-crisis/
2. http://www.bbc.com/news/world-australia-41840549
3. http://www.aljazeera.com/news/2017/11/sense-abandonment-burgeons-iraq-kurds-171102103316488.html

Instructions for verifying this message can be found at https://gibberfish.org/verify-warrant-canaries/


-----BEGIN PGP SIGNATURE-----

iQIcBAEBCgAGBQJZ+yRSAAoJEMQ4KPNkxRj6HtoP/REXjsN9yWS5pRhoqHV5qZ0O
2qk/PdpPiQ/nN4d85/tthkkw9/iapfu9DRPPbgPaoWzghl4ul0k+QPAwDjaYigy3
CBFyFfYCH35SHCcJbpkjOH9BiN3FxY+QE6pcgdRR1r/VqKPymGDt/6ExCWxrUGut
I2AGDyIu3DgBezcA9TpIu1yKJs3DPqQP+0SlbtQpKd8RhITud6YcUyOyblQAvCfK
f9Ep8tiIwAtwU1HVtD80apsJv09f9zqsr2YbBbIDpGYfq1diOS18937aSfZhKTzg
+ZXuKKqOzbBYFp6a4rG6pzokSScMeN6YiFilWR/Ra8qEFG6TAIXj3u0dvEtejmFO
CLXIsVVWbodCFaZqDN1fi9uSD4LavS2UMRJ0Hb5N2k20Potnr3CkJsIjNWneNgDq
8ov94+rfkeuT0EIBZgYuxMj8+nH+Eb1At9xOxxLGA50ueoz14oR1ZQkGxdjXlgTO
Ff6w7kF9TPFCISdLeCmIiFZgcgNaJk5ViMckp+BRApQEDnPbIZAJiAHkWl5UDjef
NtlGXFsEiKUuMQ7t48nOB9GY4D3hvpgIVWFkEfb2a7juKPId7cNyP9uQbWFSpljy
ZlZghabpnu5rILUx0HIp1y6BzQx9DS5i7Xv5M/K67Qfe3nC2n1fsq8U8kt6u0YI9
P7r/FyjAb7Ypmnr7Dbcs
=LpFK
-----END PGP SIGNATURE-----
